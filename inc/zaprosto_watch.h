#ifndef __zaprosto_watch_H__
#define __zaprosto_watch_H__

#include <watch_app.h>
#include <watch_app_efl.h>
#include <Elementary.h>
#include <dlog.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "zaprosto_watch"

#if !defined(PACKAGE)
#define PACKAGE "org.example.zaprosto_watch"
#endif

#endif /* __zaprosto_watch_H__ */
